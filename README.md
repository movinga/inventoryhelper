# Movinga Inventory Helper

## Build instructions

In order to run the application on the development machine (this is a required step of the installation process),
you will need the components listed below:

* [Node.js](https://nodejs.org/en/) as a runtime environment and npm as a package manager
(Installation details are available [here](https://nodejs.org/en/download/package-manager/))
* [Gulp](http://gulpjs.com) as a build system (`npm install gulp -g`)
* [MongoDB](https://www.mongodb.org/downloads#production) as a database (Installation details available [here](https://docs.mongodb.org/master/tutorial/install-mongodb-on-ubuntu/?_ga=1.79579728.550894883.1444930149))

After installing the required components, you should navigate to the application's
root folder and install all dependencies by running `npm install` command.

After that you need to build the application from source code by running `gulp` command.

Finally, you can run the app by going to the `build` directory and running `npm app.js` command from there.
The application is available at `http://0.0.0.0:3000`

## Installation instructions

#### Note: the installation process has been tested on Ubuntu 14.04 virtual machine.

In order to install the application on the production server, you will need the components listed below:

* [Node.js](https://nodejs.org/en/) as a runtime environment and npm as a package manager
(Installation details are available [here](https://nodejs.org/en/download/package-manager/))
* [PM2](http://pm2.keymetrics.io) as a process manager for Node.js applications (`npm install pm2 -g`)
* [MongoDB](https://www.mongodb.org/downloads#production) as a database (Installation details available [here](https://docs.mongodb.org/master/tutorial/install-mongodb-on-ubuntu/?_ga=1.79579728.550894883.1444930149))

You should copy the contents of the `build` folder from the development machine to your production server.
After that, navigate to the location of the build folder on the production server
and install all production dependencies by running `npm install --production` command.

In order to use the Amazon S3 PDF upload functionality, you should configure your Amazon S3 credentials.
To do that, simply create `~/.aws/credentials` file and insert your credentials in a format described below

```
[movinga]
aws_access_key_id = [your access key]
aws_secret_access_key = [your secret key]
```

The setup is complete, and the only thing left is to run the application using a process manager deamon
by running `pm2 start app.js` command.

The application will be available at `http:///0.0.0.0:3000`. You need a
reverse proxy to handle initial requests to the application. Example configuration file for Nginx server is provided below:

```
server {
  listen 80;

  server_name <INSERT_DOMAIN_NAME_HERE>;

  location / {
    proxy_pass http://0.0.0.0:3000;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }
}
```

## Usage notes

Currently there are two hard-coded users: **admin** and **movinga**.
Passwords are stored in plain-text (for now) at `/models/account.js.`
You can change them in this file, but you should restart the server (`pm2 restart app.js`)
in order for changes to take effect.

### Credits:
* [How To Set Up a Node.js Application for Production on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-14-04)
