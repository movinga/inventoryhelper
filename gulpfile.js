var gulp = require('gulp');
var gutil = require('gulp-util');
var notify = require('gulp-notify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var concatCss = require('gulp-concat-css');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var reactify = require('reactify');
var replace = require('gulp-replace');
var sourcemaps = require('gulp-sourcemaps');

function handleErrors() {
  gutil.log(arguments);
  var args = Array.prototype.slice.call(arguments);
  notify.onError({
    title: "Compile Error",
    message: args[0].message
  }).apply(this, args);
  this.emit('end');
}

gulp.task('default', ['compile-js', 'compile-css', 'move-files']);

gulp.task('compile-css', function() {
  gulp.src('node_modules/bootstrap/dist/fonts/*.*')
    .pipe(gulp.dest('build/public/fonts'));

  var cssFiles = [
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/flag-icon-css/css/flag-icon.min.css',
    'src/assets/css/*.css',
  ];

  gulp.src(cssFiles)
    .pipe(minifyCss())
    .pipe(replace('../../../node_modules/bootstrap/dist/', '../'))
    .pipe(gulp.dest('build/public/css'));
});

gulp.task('move-files', function() {
  gulp.src('app.js')
    .pipe(replace('build/', ''))
    .pipe(gulp.dest('build/'));

  var filesToMove = [
    'package.json',
    'src/assets/*.csv',
    'src/settings.js',
    'src/logger.js',
  ];

  gulp.src(filesToMove)
    .pipe(gulp.dest('build/'));

  gulp.src('.keep')
    .pipe(gulp.dest('build/logs'));

  gulp.src('src/views/*')
    .pipe(gulp.dest('build/views'));

  gulp.src('src/models/*')
    .pipe(gulp.dest('build/models'));

  gulp.src('src/assets/img/*.*')
    .pipe(gulp.dest('build/public/img'));

  gulp.src('src/locales/*.*')
    .pipe(gulp.dest('build/locales'));

  gulp.src('src/helpers/*.*')
    .pipe(gulp.dest('build/helpers'));

  gulp.src('src/routers/*.*')
    .pipe(gulp.dest('build/routers'))

  gulp.src('src/assets/js/*.js')
    .pipe(gulp.dest('build/public/js'));

  var flagFiles = [
    'node_modules/flag-icon-css/flags/**/gb.svg',
    'node_modules/flag-icon-css/flags/**/de.svg',
    'node_modules/flag-icon-css/flags/**/fr.svg',
    'node_modules/flag-icon-css/flags/**/it.svg'
  ];

  gulp.src(flagFiles)
    .pipe(gulp.dest('build/public/flags'));
});

gulp.task('compile-js', function() {

  var task = browserify({
    entries: 'src/frontend.js',
    debug: true,
    cache: {},
    packageCache: {},
    transform: [reactify]
  });
  task.ignore('WNdb');
  task.ignore('lapack');

  var stream = task.bundle();
  return stream
    .pipe(source('bundle.js'))
    .pipe(buffer())
    //.pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify().on('error', handleErrors))
    //.pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('build/public/js/'));
});
