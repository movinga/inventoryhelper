var express = require('express');
var app = express();
var rest = require('restler');
var fs = require('fs');
var nunjucks = require('nunjucks');
var bodyParser = require('body-parser');
var iconv = require('iconv-lite');
var cookieParser = require('cookie-parser');
var ExpressHelper = require('./build/helpers/expressHelper');
var ItemHelper = require('./build/helpers/itemHelper');
var checkAuthorized = ExpressHelper.checkAuthorized;
var routeForLocale = ExpressHelper.routeForLocale;
var InventoryHelper = require('./build/helpers/inventoryHelper');
var ILHelper = require('./build/helpers/inventoryLocaleHelper');

var InventoryRouter = require('./build/routers/inventoryRouter');
var ItemRouter = require('./build/routers/itemRouter');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');
var settings = require('./build/settings');

var mongoose = require('mongoose');
mongoose.connect(settings.mongo.connection);
var Item = require('./build/models/item');
var Inventory = require('./build/models/inventory');
var Account = require('./build/models/account');

var i18n = require('i18next');
var I18nMiddleware = require('i18next-express-middleware');
var I18nFilesystem = require('i18next-node-fs-backend');

var logger = require('./build/logger');

i18n
  .use(I18nMiddleware.LanguageDetector)
  .use(I18nFilesystem)
  .init({
    lng: 'en',
    preload: ['en', 'de', 'fr', 'it'],
    fallbackLng: 'en',
    backend: {
      loadPath: 'build/locales/{{ns}}-{{lng}}.json',
      addPath: 'build/locales/{{ns}}-{{lng}}.missing.json',
      jsonIndent: 2
    },
    detection: {
      order: ['path', 'header'],
      lookupPath: 'locale'
    }
  }, function(err, t) {
    if (err) logger.warn(err);
  });

/*
  Express configuration
*/

app.use(bodyParser.urlencoded({extended: false, limit: '5mb'}));
app.use(bodyParser.json({limit: '5mb'}));
app.use(require('express-session')({
  secret: "alkfjlsadkfjskdf",
  resave: true,
  saveUninitialized: true,
}));
app.use(cookieParser('asidhasdj'));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(I18nMiddleware.handle(i18n, {
  removeLngFromUrl: false
}));
app.set('i18next', i18n);
app.use(express.static(__dirname + '/build/public'));
app.enable('strict routing');
nunjucks.configure('build/views', {
  express: app,
  autoescape: true,
});

/*
  Passport configuration
*/

passport.use(new LocalStrategy(function(username, password, done) {
  var user = Account.find(username);
  if (!user) {
    return done(null, false, {message: i18n.t("login.errors.wrongUser") + " " + username});
  }
  if (!Account.verifyPassword(user, password)) {
    return done(null, false, {message: i18n.t("login.errors.wrongPassword")});
  }
  return done(null, user);
}));

passport.serializeUser(function(user, done) {
  return done(null, user);
});

passport.deserializeUser(function(user, done) {
  return done(null, user);
});

/*
  Main app routes
*/

app.get('/', checkAuthorized, function(req, res) {
  res.render('index.html', {user: req.user, locale: "en"});
});

app.get('/:locale', checkAuthorized, function(req, res) {
  if (req.query.hasOwnProperty('oppId') && req.query.hasOwnProperty('hash')) {
    res.render('inventory.html', {user: req.user, locale: req.params.locale, oppId: req.query.oppId, hash: req.query.hash});
  } else {
    res.render('index.html', {user: req.user, locale: req.params.locale});
  }
});

app.get('/:locale/report', function(req, res) {
  if (req.query.hasOwnProperty('oppId') && req.query.hasOwnProperty('secret-token')) {
    Inventory.findOne({oppId: req.query.oppId}, function(err, inventory) {
      if (inventory) {
        Item.populate(inventory.items, {path: 'item'}, function(err, items) {
          var localizedInventory = ILHelper.localizedInventory(inventory, req.params.locale);
          InventoryHelper.paginateInventoryItems(localizedInventory, 16, 12);
          res.render("report.html", {
            inventory: localizedInventory,
            locale: req.params.locale
          });
        });
      } else {
        res.render('index.html', {user: req.user, locale: req.params.locale});
      }
    });
  } else {
    res.render('index.html', {user: req.user, locale: req.params.locale});
  }
});

/*
  Authentication routes
*/

app.get('/:locale/login', function(req, res) {
  res.render('login.html', {
    flash: "",
    locale: req.params.locale,
    oppId: req.query.oppId ? req.query.oppId : '',
    hash: req.query.hash ? req.query.hash : ''
  });
});

app.post('/:locale/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) {return next(err);}
    if (!user) {
      if (info.message == "Missing credentials") {
        info.message = i18n.t("login.errors.emptyUser");
      }
      return res.render('login.html', {flash: info.message, locale: req.params.locale});
    }
    req.logIn(user, function(err) {
      if (err) {return next(err);}
      if (req.body.hasOwnProperty('oppId') && req.body.oppId !== '' &&
          req.body.hasOwnProperty('hash') && req.body.hash !== '') {
        return res.redirect(routeForLocale(req, '/?oppId=' + req.body.oppId + '&hash=' + req.body.hash));
      } else {
        return res.redirect(routeForLocale(req, '/'));
      }
    });
  })(req, res, next);
});

app.get('/:locale/logout', function(req, res) {
  req.logout();
  res.redirect(routeForLocale(req, ''));
});

/*
  I18n routes
*/

app.get('/:locale/translation.json', function(req, res) {
  //If does not recognize locale - go for fallback language
  if (!/^(en|fr|de|it)$/.exec(req.params.locale)) {
    req.params.locale = 'en';
  }

  var filename = __dirname + "/build/locales/translation-" + req.params.locale + ".json";
  var localeFile = fs.readFile(filename, function(err, data) {
    if (err) {
      logger.warn(err);
      res.status(500).send("Cannot process the translation file. Please, contact system administrator.");
    } else {
      var parsedString = iconv.decode(data, 'utf-8');
      res.setHeader('Content-Type', 'application/json');
      res.send(parsedString);
    }
  });
});

/*
  Other routes
*/

app.use('/items', ItemRouter);
app.use('/inventory', InventoryRouter);


var server = app.listen(3000, function() {
  var host = server.address().address;
  var port = server.address().port;

  // Upload the items from the pre-fetched array if the database is empty
  Item.find(function(err, items) {
    if (err) {
      logger.warn(err);
    } else {
      if (items.length === 0) {
        logger.info('There are 0 items in the database, populating the database from built-in items.csv file');
        var filename = __dirname + '/build/input.csv';
        fs.readFile(filename, function(err, data) {
          if (err) {
            logger.warn('Cannot read %s file!', filename);
          } else {
            ItemHelper.importItems(data, false, function() {
              logger.info('Database populating is complete!');
            });
          }
        });
      } else {
        logger.info('There are %s items in the database, no need to populate from file', items.length);
      }
    }
  });

  console.log('Application started at http://%s:%s', host, port);
  logger.info('Application started at http://%s:%s', host, port);
});

module.exports = app;
