var MongoClient = require('mongodb').MongoClient;
var async = require('async');

var url = 'mongodb://localhost/test';
MongoClient.connect(url, function(err, db) {
  if (err) {
    console.error(err);
  } else {
    patchItems(db, function() {
      patchInventories(db, function() {
        db.close();
      });
    });
  }
});

function patchItems(db, callback) {
  var collection = db.collection('items');
  collection.find({}).toArray(function(err, items) {
    if (err) {
      console.error(err)
    } else {
      async.eachSeries(items, function(item, next) {
        var updated = false;
        var changes = {}
        if (item.weight instanceof Object) {
          changes.weight = item.weight.lb;
          updated = true;
        }
        if (item.volume instanceof Object) {
          changes.volume = item.volume.cf;
          updated = true;
        }
        if (item.assemblyPrice instanceof Object) {
          changes.assemblyPrice = item.assemblyPrice.GBP;
          updated = true;
        }
        if (!item.hasOwnProperty('disassembledVolume')) {
          changes.disassembledVolume = item.volume;
          updated = true;
        }
        if (updated) {
          collection.updateOne({'_id': item['_id']}, {$set: changes}, function(err) {
            if (err) {
              console.error(err);
            }
            next();
          });
        } else {
          next();
        }
      }, function() {
        callback();
      });
    }
  });
}

function patchInventories(db, callback) {
  var collection = db.collection('inventories');
  collection.find({}).toArray(function(err, inventories) {
    if (err) {
      console.error(err)
    } else {
      async.eachSeries(inventories, function(inventory, next) {
        var updated = false;
        var changes = {}
        if (inventory.totalWeight instanceof Object) {
          changes.totalWeight = inventory.totalWeight.lb;
          updated = true;
        }
        if (inventory.totalVolume instanceof Object) {
          changes.totalVolume = inventory.totalVolume.cf;
          updated = true;
        }
        if (inventory.totalAssemblyPrice instanceof Object) {
          changes.assemblyPrice = inventory.totalAssemblyPrice.GBP;
          updated = true;
        }
        if (updated) {
          collection.updateOne({'_id': inventory['_id']}, {$set: changes}, function(err) {
            if (err) {
              console.error(err);
            }
            next();
          });
        } else {
          next();
        }
      }, function() {
        callback();
      });
    }
  });
}
