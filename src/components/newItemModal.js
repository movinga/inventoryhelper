var React = require('react');
var ItemActions = require('../actions/itemActions');
var InventoryActions = require('../actions/inventoryActions');

var Formsy = require('formsy-react');
var FRC = require('formsy-react-components');


var natural = require('natural');

var NewItemModal = React.createClass({
  propTypes: {
    items: React.PropTypes.array.isRequired,
    itemName: React.PropTypes.string.isRequired,
    locale: React.PropTypes.string.isRequired,
    suggestions: React.PropTypes.array,
    disabled: React.PropTypes.bool,
  },

  mixins: [],

  getInitialState: function() {
    return {
      validationErrors: [],
      disabled: true,
      similarItems: [],
    }
  },

  componentWillReceiveProps: function(newProps) {
    //Set the value of the item and build suggestions.
    similarityArray = []
    var tokenizer = new natural.WordTokenizer();
    newProps.items.forEach(function(item) {
      var lowerCaseName = item.name[newProps.locale].toLowerCase();
      var lowerCaseItemName = newProps.itemName.toLowerCase();
      var words = tokenizer.tokenize(lowerCaseName);
      var distance = natural.JaroWinklerDistance(words.join(' '), lowerCaseItemName);
      var reversedDistance = natural.JaroWinklerDistance(words.reverse().join(' '), lowerCaseItemName);
      similarityArray.push({
        index: similarityArray.length,
        similarity: Math.min(distance, reversedDistance)
      });
    });
    similarityArray.sort(function(a, b) {
      return b.similarity - a.similarity;
    });
    this.setState({
      similarItems: similarityArray.slice(0, 10)
    });
  },

  submitItem: function(model, resetForm, invalidateForm) {
    // Check is the item is unique
    var item = this.props.items.find(function(element) {
      return element.name[this.props.locale] === model.name;
    }.bind(this));
    if (item) {
      // Item found, return an error
      alert("Item already exists!");
    } else {
      // Add item to database and to inventory.
      ItemActions.submit({
        name: model.name,
        locale: this.props.locale
      }, function() {
        this.resetForm();
        this.setState({
          validationErrors: [],
          disabled: true,
          similarItems: [],
        }, function() {
          $("#addItemModal").modal('toggle');
          $('.typeahead').typeahead('val','');
          $("#itemNameInput").focus();
          $("#itemQuantityInput").val("1");
        });
      }.bind(this));
    }
  },

  addItem: function(item) {
    // Add existing item to the inventory
    InventoryActions.addItem({
      'item': item,
      'quantity': 1,
      'disassembly': false,
      'reassembly': false
    });
    this.resetForm();
    this.setState({
      validationErrors: [],
      disabled: true,
      similarItems: [],
    }, function() {
      $("#addItemModal").modal('toggle');
      $('.typeahead').typeahead('val','');
      $("#itemNameInput").focus();
      $("#itemQuantityInput").val("1");
    });
  },

  enableForm: function() {
    this.setState({
      disabled: false
    });
  },

  disableForm: function() {
    this.setState({
      disabled: true
    });
  },

  resetForm: function() {
    this.refs.AddItemForm.reset();
  },

  render: function() {
    var suggestions = function() {
      var items = [];
      var props = this.props;
      var addItem = this.addItem;
      this.state.similarItems.forEach(function(item, i) {
        items.push(<a href='#' key={'suggestion-' + item.index} className="btn btn-block btn-default btn-sm" onClick={function() {addItem(props.items[item.index])}}>{props.items[item.index].name[props.locale]}</a>);
      });
      return items;
    }.bind(this);

    return (
      <div className="modal fade" id="addItemModal" tabIndex="-1" role="dialog" aria-labelledby="addItemModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal"
                aria-label="Close"><span aria-hidden="true" tabIndex="-1" className="glyphicon glyphicon-remove"></span></button>
              <h4 className="modal-title" id="addItemModalLabel">{t("addItemModal.title")}</h4>
            </div>
            <div className="modal-body">
              {t("addItemModal.message.line1")}<br/>
              {t("addItemModal.message.line2")}
              {suggestions()}
              If you are not satisfied with the suggestions, you can add the item to inventory.
              <Formsy.Form onValidSubmit={this.submitItem} onValid={this.enableForm}
                onInvalid={this.disableForm} validatePristine="true" className="form-horizontal" ref="AddItemForm">
                <FRC.Checkbox
                  name="agree"
                  value={false}
                  label="I assume that this item really necessary"
                  validations="equals:true"
                  validationErrors="You must agree with that statement."
                />
                <FRC.Input
                  name="name"
                  value={this.props.itemName}
                  placeholder="Item name"
                  label="Item name"
                  required
                />
                <FRC.Input
                  name="locale"
                  value={this.props.locale}
                  type="hidden"
                />
              <button type="submit" className="btn btn-primary btn-block" disabled={this.state.disabled}>
                  Add item
                </button>
              </Formsy.Form>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = NewItemModal;
