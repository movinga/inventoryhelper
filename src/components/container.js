var React = require('react');
var Reflux = require('reflux');
var ItemActions = require('../actions/itemActions');
var ItemStore = require('../stores/itemStore');

var InventoryActions = require('../actions/inventoryActions');
var InventoryStore = require('../stores/inventoryStore');

var ItemSearch = require('./itemSearch');
var ItemTable = require('./itemTable');

var Container = React.createClass({

  propTypes: {
    items: React.PropTypes.array,
    inventory: React.PropTypes.object,
    locale: React.PropTypes.string.isRequired,
    oppId: React.PropTypes.string.isRequired
  },

  mixins: [
    Reflux.listenTo(ItemStore, 'onItemsChanged'),
    Reflux.listenTo(InventoryStore, 'onInventoryChanged'),
  ],

  getInitialState: function() {
    ItemActions.fetch();
    InventoryActions.fetch();
    return ({
      items: undefined,
      inventory: undefined,
    });
  },

  onItemsChanged: function(items) {
    this.setState({items: items});
  },

  onInventoryChanged: function(inventory) {
    this.setState({inventory: inventory});
  },

  render: function() {
    //Wait until items and inventory are fetched.
    if (this.state.items && this.state.inventory) {
      return (
        <div className="row">
          <div className="col-md-10 col-md-offset-1">
            <h2>{t("container.title")} {this.props.oppId}</h2>
            <ItemSearch locale={this.props.locale} items={this.state.items} />
            <ItemTable locale={this.props.locale} inventory={this.state.inventory} />
          </div>
        </div>
      );
    } else {
      return(<div></div>);
    }
  }
});

module.exports = Container;
