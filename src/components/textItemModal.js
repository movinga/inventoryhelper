var React = require('react');
var TextItemRow = require('./textItemRow');
var InventoryItemLabels = require('../mixins/invItemLabels');
var InventoryLabels = require('../mixins/inventoryLabels');
var ExcelBuilderMixin = require('../mixins/excelBuilder');

function convert(s) {
  var buffer = new ArrayBuffer(s.length);
  var view = new Uint8Array(buffer);
  for (var i = 0; i != s.length; ++i) {
    view[i] = s.charCodeAt(i) & 0xFFf;
  }
  return buffer;
}

var TextItemModal = React.createClass({

  propTypes: {
    inventory: React.PropTypes.object.isRequired,
    extended: React.PropTypes.bool,
    locale: React.PropTypes.string.isRequired
  },

  mixins: [
    ExcelBuilderMixin,
    InventoryLabels,
    InventoryItemLabels
  ],

  getInitialState: function() {
    return {
      extended: false
    }
  },

  exportItems: function() {
    var blob = new Blob([convert(this.createWorksheet())]);
    saveAs(blob, "test.xlsx");
  },

  setExpanded: function() {
    jQuery('#expandedModeButton').addClass('active');
    jQuery('#compactModeButton').removeClass('active');
    this.setState({
      extended: true
    });
  },

  setCompact: function() {
    jQuery('#expandedModeButton').removeClass('active');
    jQuery('#compactModeButton').addClass('active');
    this.setState({
      extended: false
    });
  },

  render: function() {
    var inventoryRows = function() {
      var rows = [];
      this.props.inventory.items.forEach(function(element, index, array) {
        rows.push(<TextItemRow locale={this.props.locale} key={index} invItem={element} extended={this.state.extended} />)
      }.bind(this));
      return rows;
    }.bind(this);

    if (this.state.extended) {
      extendedHeader = [
          (<th key='head0' className="col-xs-1">{t("inventoryTable.header.weight")}</th>),
          (<th key='head2' className="col-xs-1">{t("inventoryTable.header.volume")}</th>)
      ];
      extendedFooter = (
        <tfoot>
          <tr>
            <th colSpan="2">{t("inventoryTable.footer.total")}:</th>
            <th>{this.totalWeightText()}</th>
            <th>{this.totalVolumeText()}</th>
            <th>{this.totalAssemblyPriceText()}</th>
          </tr>
        </tfoot>
      );
    } else {
      extendedHeader = [];
      extendedFooter = [];
    }

    return (
      <div className="modal fade" id="reportModal" tabIndex="-1" role="dialog" aria-labelledby="reportModalLabel">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal"
                aria-label="Close"><span aria-hidden="true" className="glyphicon glyphicon-remove"></span></button>
              <h4 className="modal-title" id="reportModalLabel">{t("reportModal.title")}</h4>
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <th className="col-xs-5">{t("inventoryTable.header.item")}</th>
                    <th className="col-xs-2">{t("inventoryTable.header.quantity")}</th>
                    {extendedHeader}
                    <th className="col-xs-3">{t("inventoryTable.header.notes")}</th>
                  </tr>
                </thead>
                <tbody>
                  {inventoryRows()}
                </tbody>
                {extendedFooter}
              </table>
            </div>
            <div className="modal-footer">
              <div className="pull-left">
                <div className="btn-group" role="group">
                  <button id="compactModeButton" type="button" className="btn btn-default active" onClick={this.setCompact}>{t("reportModal.compactMode")}</button>
                  <button id="expandedModeButton" className="btn btn-default" onClick={this.setExpanded}>{t("reportModal.expandedMode")}</button>
                </div>
              </div>
              <button type="button" className="btn btn-success" onClick={this.exportItems} data-dismiss="modal">{t("reportModal.exportXLSButton")}</button>
              <button type="button" className="btn btn-primary" data-dismiss="modal">{t("reportModal.closeButton")}</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = TextItemModal;
