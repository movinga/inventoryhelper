var React = require('react');
var Reflux = require('reflux');
var ItemRow = require('./itemRow');
var TextItemModal = require('./textItemModal');
var InventoryLabels = require('../mixins/inventoryLabels');
var InventoryActions = require('../actions/inventoryActions');
var LinkedStateMixin = require('react-addons-linked-state-mixin');

var ItemTable = React.createClass({

  propTypes: {
    inventory: React.PropTypes.object.isRequired,
    locale: React.PropTypes.string.isRequired,
    comment: React.PropTypes.string
  },

  mixins: [
    InventoryLabels,
    LinkedStateMixin
  ],

  getInitialState: function() {
    return {
      comment: this.props.inventory.comment ? this.props.inventory.comment : ''
    }
  },

  saveInventory: function() {
    InventoryActions.save(this.props.inventory, this.state.comment);
  },

  render: function() {
    var inventoryRows = function() {
      var rows = [];
      this.props.inventory.items.forEach(function(element, index, array) {
        rows.push(<ItemRow locale={this.props.locale} key={index} id={index} invItem={element} />);
      }.bind(this));
      return rows;
    }.bind(this);

    return (
      <div>
        <h5>Comment:</h5>
        <div className="form-group">
          <textarea rows="5" className="form-control" valueLink={this.linkState('comment')}></textarea>
        </div>
        <h3>{t("inventoryTable.title")}:</h3>
        <table className="table table-condensed">
          <thead>
            <tr>
              <th className="col-xs-5">{t("inventoryTable.header.item")}</th>
              <th className="col-xs-2">{t("inventoryTable.header.quantity")}</th>
              <th className="col-xs-1">{t("inventoryTable.header.weight")}</th>
              <th className="col-xs-1">{t("inventoryTable.header.volume")}</th>
              <th className="col-xs-1">{t("inventoryTable.header.disassembly")}</th>
              <th className="col-xs-1">{t("inventoryTable.header.reassembly")}</th>
              <th className="col-xs-1">{t("inventoryTable.header.delete")}</th>
            </tr>
          </thead>
          <tbody>
            {inventoryRows()}
          </tbody>
          <tfoot>
            <tr>
              <th colSpan="2">{t("inventoryTable.footer.total")}:</th>
              <th>{this.totalWeightText()}</th>
              <th>{this.totalVolumeText()}</th>
              <th colSpan="2">{this.totalAssemblyPriceText()}</th>
              <th></th>
            </tr>
          </tfoot>
        </table>
        <div className="row">
          <div className="col-md-5 col-md-offset-7">
            <div className="row">
              <div className="col-xs-6">
                <button type="button" className="btn btn-primary btn-block" onClick={this.saveInventory} data-dismiss="modal">Save inventory</button>
              </div>
              <div className="col-xs-6">
                <button className="btn btn-success btn-block" data-toggle="modal"
                  data-target="#reportModal">{t("inventoryTable.footer.generateButton")}</button>
              </div>
            </div>
          </div>
        </div>
        <TextItemModal locale={this.props.locale} inventory={this.props.inventory} />
      </div>
    );
  }
});

module.exports = ItemTable;
