var React = require('react');
var InventoryItemLabels = require('../mixins/invItemLabels');

var TextItemRow = React.createClass({

  propTypes: {
    invItem: React.PropTypes.object.isRequired,
    extended: React.PropTypes.bool.isRequired,
    locale: React.PropTypes.string.isRequired
  },

  mixins: [
    InventoryItemLabels,
  ],

  render: function() {
    if (this.props.extended) {
      var extendedColumns = [
        (<td key='weight'>{this.weightText(this.props.invItem, true)}</td>),
        (<td key='volume'>{this.volumeText(this.props.invItem, true)}</td>)
      ];
    } else {
      var extendedColumns = [];
    }

    return (
      <tr>
        <td>{this.props.invItem.item.name[this.props.locale]}</td>
        <td>
          {(this.props.invItem.quantity > 1 || this.props.extended) ?
            this.typeText(this.props.invItem, true) : ''}
        </td>
        {extendedColumns}
        <td>{this.notesText(this.props.invItem, this.props.extended)}</td>
      </tr>
    );
  }
});

module.exports = TextItemRow;
