var React = require('react');
var InventoryActions = require('../actions/inventoryActions');
var InventoryItemLabels = require('../mixins/invItemLabels');
var LocaleHelper = require('../helpers/localeHelper');

var ItemRow = React.createClass({

  propTypes: {
    invItem: React.PropTypes.object.isRequired,
    id: React.PropTypes.number.isRequired,
    locale: React.PropTypes.string.isRequired,
  },

  mixins: [
    InventoryItemLabels,
  ],

  updateQuantity: function(event) {
    InventoryActions.updateItem(this.props.id, {
      quantity: event.target.value,
      disassembly: this.props.invItem.disassembly,
      reassembly: this.props.invItem.reassembly
    });
  },

  updateDisassembly: function(event) {
    InventoryActions.updateItem(this.props.id, {
      quantity: this.props.invItem.quantity,
      disassembly: event.target.checked,
      reassembly: this.props.invItem.reassembly
    });
  },

  updateReassembly: function(event) {
    InventoryActions.updateItem(this.props.id, {
      quantity: this.props.invItem.quantity,
      disassembly: this.props.invItem.disassembly,
      reassembly: event.target.checked
    });
  },

  removeItem: function() {
    InventoryActions.removeItem(this.props.id);
  },

  disassemblyCheckbox: function() {
    if (!this.props.invItem.item.new && this.props.invItem.item.assemblyPrice === 0) {
      return (<div></div>);
    } else {
      return (
        <div className="checkbox">
          <label>
            <input type="checkbox" aria-label="..." onChange={this.updateDisassembly} checked={this.props.invItem.disassembly} />
          </label>
        </div>
      );
    }
  },

  reassemblyCheckbox: function() {
    if (!this.props.invItem.item.new && this.props.invItem.item.assemblyPrice === 0) {
      return (<div></div>);
    } else {
      return (
        <div className="checkbox">
          <label>
            <input type="checkbox" aria-label="..." onChange={this.updateReassembly} checked={this.props.invItem.reassembly} />
          </label>
        </div>
      );
    }
  },

  render: function() {
    return (
      <tr>
        <td className="vert-align">{this.props.invItem.item.name[this.props.locale]}</td>
        <td className="vert-align">
          <div className="input-group input-group-sm">
            <input type="number" min="1" max="1000" className="form-control"
              value={this.props.invItem.quantity}
              onChange={this.updateQuantity} aria-describedby={"quantity-" + this.props.id}/>
            <span className="input-group-addon" id={"quantity-" + this.props.id}>
              {this.typeText(this.props.invItem, false)}
            </span>
          </div>
        </td>
        <td className="vert-align">
          {this.weightText(this.props.invItem, true)}
        </td>
        <td className="vert-align">
          {this.volumeText(this.props.invItem, true)}
        </td>
        <td className="vert-align hor-align">
          {this.disassemblyCheckbox()}
        </td>
        <td className="vert-align hor-align">
          {this.reassemblyCheckbox()}
        </td>
        <td className="vert-align hor-align">
          <button className="btn btn-sm btn-default" onClick={this.removeItem}>
            <span className="glyphicon glyphicon-remove"></span>
          </button>
        </td>
      </tr>
    );
  }
});

module.exports = ItemRow;
