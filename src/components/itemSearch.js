var React = require('react');
var NewItemModal = require('./newItemModal');
var InventoryActions = require('../actions/inventoryActions');
var Bloodhound = require("typeahead.js/dist/bloodhound.js");
var ItemLabelsMixin = require('../mixins/itemLabels');

require('typeahead.js');

function buildSuggestions(items, locale) {
  var typeaheadItems = []
  items.forEach(function(item) {
    if (item.name[locale] !== '') {
      typeaheadItems.push(item.name[locale]);
    }
  });

  var suggestions = new Bloodhound({
    datumTokenizer: function(datum) {
      datum = datum.replace(/([.?*+^$[\]\\(){}|-])/g, "");
      return Bloodhound.tokenizers.whitespace(datum);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: typeaheadItems,
  });

  suggestions.initialize();

  return suggestions;
}

var ItemSearch = React.createClass({

  propTypes: {
    selectedItem: React.PropTypes.object,
    items: React.PropTypes.array.isRequired,
    itemNameInput: React.PropTypes.string,
    quantityInput: React.PropTypes.number,
    locale: React.PropTypes.string.isRequired,
  },

  mixins: [
    ItemLabelsMixin,
  ],

  getInitialState: function() {
    return({
      selectedItem: undefined,
      itemNameInput: "",
      itemTypeText: this.itemTypeText(undefined, 1),
      quantityInput: 1,
    });
  },

  componentWillReceiveProps: function(nextProps) {
    var suggestions = buildSuggestions(nextProps.items, nextProps.locale);
    $("#itemNameInput").typeahead('destroy');
    $("#itemNameInput").typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'items',
      source: suggestions,
      limit: 10
    }).on('typeahead:select', function(event, selection) {
      this.handleItemNameChange(event);
    }.bind(this));
  },

  componentDidMount: function() {
    var suggestions = buildSuggestions(this.props.items, this.props.locale);

    $("#itemNameInput").typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'items',
      source: suggestions,
      limit: 10
    }).on('typeahead:select', function(event, selection) {
      this.handleItemNameChange(event);
    }.bind(this));
  },

  addToInventory: function() {
    // Add the item listed in this.state.selectedItem to inventory
    if (this.state.itemNameInput !== '') {
      if (this.state.selectedItem) {
        InventoryActions.addItem({
          item: this.state.selectedItem,
          quantity: this.state.quantityInput,
          disassembly: false,
          reassembly: false
        });
        this.setState({
          selectedItem: undefined,
          quantityInput: 1,
          itemNameInput: '',
          itemTypeText: this.itemTypeText(undefined, 1)
        }, function() {
          $('.typeahead').typeahead('val','');
          $("#itemNameInput").focus();
          $("#itemQuantityInput").val("1");
        });
      } else {
        // Open newItemModal
        $("#addItemModal").modal('toggle');
      }
    }
  },

  findItemByName: function(name) {
    return this.props.items.filter(function(item) {
      return item.name[this.props.locale] === name;
    }.bind(this))[0];
  },

  handleItemNameChange: function(event) {
    // Only update the state value here
    var keyCode = (event.keyCode ? event.keyCode : event.which);

    var checkCode = function() {
      if (keyCode == 13) {
        this.addToInventory();
      }
    };

    var item = this.findItemByName(event.target.value);
    if (item === undefined) {
      var firstSuggestion = jQuery(".tt-suggestion:first-child").text();
      var firstSuggestionItem = this.findItemByName(firstSuggestion);
      if (firstSuggestion === undefined || firstSuggestion === "") {
        this.setState({
          selectedItem: undefined,
          itemNameInput: event.target.value,
          itemTypeText: this.itemTypeText(undefined, this.state.quantityInput)
        }, checkCode);
      } else {
        this.setState({
          selectedItem: firstSuggestionItem,
          itemNameInput: firstSuggestionItem.name[this.props.locale],
          itemTypeText: this.itemTypeText(firstSuggestionItem, this.state.quantityInput)
        }, checkCode);
      }
    } else {
      this.setState({
        selectedItem: item,
        itemNameInput: item.name[this.props.locale],
        itemTypeText: this.itemTypeText(item, this.state.quantityInput)
      }, checkCode);
    }
  },

  handleQuantityChange: function(event) {
    var keyCode = (event.keyCode ? event.keyCode : event.which);
    this.setState({
      quantityInput: parseInt(event.target.value),
      itemTypeText: this.itemTypeText(this.state.selectedItem, this.state.quantityInput)
    }, function() {
      if (keyCode == 13) {
        this.addToInventory();
      }
    }.bind(this));
  },

  render: function() {
    return (
      <div>
        <h3>{t("itemSearch.title")}:</h3>
        <div className="row">
          <div className="col-xs-6">
            <div id="bloodhound">
              <input type="text" className="form-control typeahead"
                placeholder={t("itemSearch.placeholder") + "..."} id="itemNameInput" onKeyUp={this.handleItemNameChange}/>
            </div>
          </div>
          <div className="col-xs-3">
            <div className="input-group">
              <input type="number" min="1" max="1000" className="form-control"
                defaultValue={this.state.quantityInput} onKeyUp={this.handleQuantityChange} onChange={this.handleQuantityChange}
                aria-describedby="itemQuantityInputDesc" id="itemQuantityInput"/>
              <span className="input-group-addon" id="itemQuantityInputDesc">{this.state.itemTypeText}</span>
            </div>
          </div>
          <div className="col-xs-3">
            <button className="btn btn-block btn-primary" onClick={this.addToInventory}>{t("itemSearch.addItemButton")}</button>
          </div>
        </div>
        <NewItemModal items={this.props.items} locale={this.props.locale} itemName={this.state.itemNameInput} />
    </div>
    );
  }
});

module.exports = ItemSearch;
