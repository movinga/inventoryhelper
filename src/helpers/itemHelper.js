var Papa = require('papaparse');
var iconv = require('iconv-lite');
var logger = require('../logger')

var UtilityHelper = require('./utilityHelper');
var LocaleHelper = require('./localeHelper');
var round = UtilityHelper.round;
var Item = require('../models/item');

function exportItems(writeStream, items) {
  // Header
  writeStream.write([
    '',
    'UKitemname',
    'itemVolumeCF',
    'itemVolumeCM',
    'itemVolumeCF (Disassembled)',
    'itemVolumeCM (Disassembled)',
    'itemWeightPounds',
    'itemWeightKG',
    'itemType',
    'assemblyPriceUK',
    'assemblyPriceDE/FR/IT',
    'DEitemname',
    'FRitemname',
    'ITitemname',
    'new'
  ].join('\t') + '\n');
  items.forEach(function(item) {
    writeStream.write([
      '',
      item.name.en,
      item.volume,
      LocaleHelper.toCM(item.volume),
      item.disassembledVolume,
      LocaleHelper.toCM(item.disassembledVolume),
      item.weight,
      LocaleHelper.toKG(item.weight),
      item.type,
      item.assemblyPrice,
      LocaleHelper.toEUR(item.assemblyPrice),
      item.name.de,
      item.name.fr,
      item.name.it,
      item.new
    ].join('\t') + '\n');
  });
}

function importItems(data, upsert, callback) {
  var parsedString = iconv.decode(data, 'utf8');
  Papa.parse(parsedString, {
    header: true,
    skipEmptyLines: true,
    complete: function(results) {
      results.data.forEach(function(result) {
        // Start building the item.
        var item = {
          name: {
            en: result.UKitemname ? result.UKitemname : '',
            de: result.DEitemname ? result.DEitemname : '',
            fr: result.FRitemname ? result.FRitemname : '',
            it: result.ITitemname ? result.ITitemname : ''
          },
          type: result.itemType,
          new: result.new === 'true' ? true : false,
          volume:  parseFloat(result.itemVolumeCF.replace(',', '.')),
          disassembledVolume: parseFloat(result['itemVolumeCF (Disassembled)'].replace(',', '.')),
          weight: parseFloat(result.itemWeightPounds.replace(',', '.')),
          assemblyPrice: parseFloat(result.assemblyPriceUK.replace(',', '.')),
        };
        insertItem(item, upsert);
      });
      callback();
    }
  });
}

function insertItem(item, upsert) {
  //Item validation
  if (!isNaN(item.volume) && !isNaN(item.disassembledVolume) &&
    item.type && !isNaN(item.weight) && !isNaN(item.assemblyPrice)) {
      if (upsert) {
        var query = {
          $or: []
        }
        if (item.name.en !== '') {
          query['$or'].push({'name.en': item.name.en})
        }
        if (item.name.de !== '') {
          query['$or'].push({'name.de': item.name.de})
        }
        if (item.name.fr !== '') {
          query['$or'].push({'name.fr': item.name.fr})
        }
        if (item.name.it !== '') {
          query['$or'].push({'name.it': item.name.it})
        }
        Item.findOneAndUpdate(query, item, {upsert: true}, function(err) {
          if (err) logger.warn(err);
        });
      } else {
        var itemObject = new Item(item);
        itemObject.save(function(err) {
          if (err) logger.warn(err);
        });
      }
    }
}

module.exports = {
  exportItems: exportItems,
  importItems: importItems,
  insertItem: insertItem
}
