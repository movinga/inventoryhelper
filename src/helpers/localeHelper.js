var UtilityHelper = require('./utilityHelper');
var round = UtilityHelper.round;

function volume(volume, locale) {
  return locale === 'en' ? volume : toCM(volume);
}

function weight(weight, locale) {
  return locale === 'en' ? weight : toKG(weight);
}

function currency(currency, locale) {
  return locale === 'en' ? currency : toEUR(currency);
}

function toCM(volume) {
  return round(volume / 35.3147, 2);
}

function toKG(weight) {
  return round(weight * 0.453592, 2);
}

function toEUR(price) {
  return round(price * 1.3, 2);
}

module.exports = {
  volume: volume,
  weight: weight,
  currency: currency,
  toCM: toCM,
  toKG: toKG,
  toEUR: toEUR
}
