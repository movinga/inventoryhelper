var phantom = require('phantom');
var rest = require('restler');
var fs = require('fs');
var settings = require('../settings');
var logger = require('../logger');

var AWS = require('aws-sdk');
var credentials = new AWS.SharedIniFileCredentials({profile: settings.aws.profile});
AWS.config.credentials = credentials;
AWS.config.region = settings.aws.region;
var s3 = new AWS.S3();
var ILHelper = require('./inventoryLocaleHelper');

function paginateInventoryItems(inventory, pageSize, lastPageSize) {
  if (inventory.items.length > 0) {
    var paginatedItems = []
    var i, j = 0
    for (i = 0, j = inventory.items.length; i < j; i+= pageSize) {
      paginatedItems.push(inventory.items.slice(i, i + pageSize))
    }
    if (paginatedItems[paginatedItems.length - 1].length > lastPageSize) {
      var left = paginatedItems[paginatedItems.length - 1].slice(0, lastPageSize);
      var right = paginatedItems[paginatedItems.length - 1].slice(lastPageSize, pageSize)
      paginatedItems.pop()
      paginatedItems.push(left);
      paginatedItems.push(right);
    }
    inventory.items = paginatedItems;
  } else {
    logger.debug('Someone saved an empty inventory.');
  }
}

function createInventoryReport(locale, oppId, callback) {
  phantom.create().then(function(ph) {
    ph.createPage().then(function(page) {
      page.property('paperSize', {width: '210mm', height: '297mm', margin: '0px'}).then(function() {
        var url = 'http:/0.0.0.0:3000/' + locale + '/report?secret-token=keyboardcat&oppId=' + oppId;
        page.open(url).then(function(status) {
          var filename = __dirname + '/' + oppId + '-' + locale + '.pdf';
          page.render(filename).then(function() {
            callback(filename);
            page.close();
            ph.exit();
          });
        });
      });
    });
  });
}

function uploadInventoryReport(filename, locale, hash, callback) {
  fs.access(filename, fs.F_OK, function(err) {
    if (!err) {
      s3.putObject({
        Bucket: settings.aws.bucketName,
        Key: locale + '/' + hash + '.pdf',
        Body: fs.createReadStream(filename),
        ContentType: 'application/pdf'
      }, function(err, data) {
        if (err) logger.warn(err.toString('utf8'));
        else {
          logger.info('Successfully uploaded file %s!', filename);
          callback(filename);
        }
      });
    } else {
      logger.warn("%s is not accessible!", filename);
    }
  });
}

function triggerInventoryCallback(inventory, locale, i18n) {
  var localizedInventory = ILHelper.localizedInventory(inventory, locale);
  var disassemblyItems = []
  var reassemblyItems = []
  localizedInventory.items.forEach(function(invItem) {
    if (invItem.disassembly === 'true') {
      disassemblyItems.push(invItem.name);
    }
    if (invItem.reassembly === 'true') {
      reassemblyItems.push(invItem.name);
    }
  });

  var t = i18n.getFixedT(locale);

  var data = {
    oppId: localizedInventory.oppId,
    totalWeight: localizedInventory.totalWeight,
    weightUnit: t("units.weight"),
    totalVolume: localizedInventory.totalVolume,
    volumeUnit: t("units.extendedVolume"),
    disassemblyItems: disassemblyItems,
    reassemblyItems: reassemblyItems,
    createdAt: new Date(),
  }

  rest.get(settings.zapier.webhookURL, {
    data: JSON.stringify(data),
    timeout: 10000,
  }).on('complete', function(data, response) {
    logger.debug("Received Zapier response.");
  }).on('timeout', function() {
    console.log("TIMEOUT!");
  });
}

module.exports = {
  paginateInventoryItems: paginateInventoryItems,
  createInventoryReport: createInventoryReport,
  uploadInventoryReport: uploadInventoryReport,
  triggerInventoryCallback: triggerInventoryCallback
}
