function round(value, digits) {
  var expPlus = 'e+' + digits;
  var expMinus = 'e-' + digits;
  return +(Math.round(value + expPlus) + expMinus);
}

module.exports = {
  round: round
}
