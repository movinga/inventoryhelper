var LocaleHelper = require('./localeHelper');
var round = require('./utilityHelper').round;

function localizedInventory(inventory, locale) {
  var localizedInventory = {
    oppId: inventory.oppId,
    comment: inventory.comment,
    totalWeight: localizedTotalWeight(inventory, locale),
    totalVolume: localizedTotalVolume(inventory, locale),
    totalAssemblyPrice: localizedTotalAssemblyPrice(inventory, locale),
    items: []
  }

  inventory.items.forEach(function(invItem) {
    var item = invItem.item;
    localizedInventory.items.push({
      name: item.name[locale],
      quantity: invItem.quantity,
      weight: LocaleHelper.weight(item.weight, locale),
      volume: LocaleHelper.volume(item.volume, locale),
      disassembly: invItem.disassembly,
      reassembly: invItem.reassembly
    });
  });

  return localizedInventory;
}

function localizedTotalVolume(inventory, locale) {
  if (locale === 'en') {
    return inventory.totalVolume
  } else {
    var volume = 0;
    inventory.items.forEach(function(invItem) {
      if (invItem.disassembly) {
        volume = round(volume + LocaleHelper.toCM(invItem.item.disassembledVolume) * invItem.quantity, 2);
      } else {
        volume = round(volume + LocaleHelper.toCM(invItem.item.volume) * invItem.quantity, 2);
      }
    });
    return volume;
  }
}

function localizedTotalWeight(inventory, locale) {
  if (locale === 'en') {
    return inventory.totalWeight
  } else {
    var weight = 0;
    inventory.items.forEach(function(invItem) {
      weight = round(weight + LocaleHelper.toKG(invItem.item.weight) * invItem.quantity, 2);
    });
    return weight;
  }
}

function localizedTotalAssemblyPrice(inventory, locale) {
  if (locale === 'en') {
    return inventory.totalAssemblyPrice
  } else {
    var assemblyPrice = 0;
    inventory.items.forEach(function(invItem) {
      assemblyPrice = round(assemblyPrice + LocaleHelper.toEUR(invItem.item.assemblyPrice) * invItem.quantity * (invItem.disassembly + invItem.reassembly), 2);
    });
    return assemblyPrice;
  }
}

module.exports = {
  localizedInventory: localizedInventory,
  localizedTotalVolume: localizedTotalVolume,
  localizedTotalWeight: localizedTotalWeight,
  localizedTotalAssemblyPrice: localizedTotalAssemblyPrice
}
