function checkAuthorized(req, res, next) {
  if (req.user) {
    next();
  } else if (req.query.hasOwnProperty('oppId') && req.query.hasOwnProperty('hash')) {
    res.redirect(routeForLocale(req, '/login?oppId=' + req.query.oppId + '&hash=' + req.query.hash));
  } else {
    res.redirect(routeForLocale(req, '/login'));
  }
}

function checkAuthorizedAPI(req, res, next) {
  if (req.user) {
    next();
  } else {
    res.status(401).send("You are not authorized to perform this action.");
  }
}

function routeForLocale(req, endpoint) {
  return req.params.locale ? '/' + req.params.locale + endpoint : '/en' + endpoint;
}

module.exports = {
  checkAuthorizedAPI: checkAuthorizedAPI,
  checkAuthorized: checkAuthorized,
  routeForLocale: routeForLocale
}
