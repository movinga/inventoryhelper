module.exports = {
  aws: {
    region: 'eu-central-1',
    profile: 'movinga',
    bucketName: 'movinga-mail-attachments',
    //profile: 'default',
    //bucketName: 'zeldin.pro-inventory.items'
  },
  zapier: {
    webhookURL: 'https://zapier.com/hooks/catch/2iokfo/'
  },
  mongo: {
    connection: 'mongodb://localhost/test'
  }
}
