var express = require('express');
var bodyParser = require('body-parser');
var ExpressHelper = require('../helpers/expressHelper');
var fs = require('fs');
var ItemHelper = require('../helpers/itemHelper');
var Item = require('../models/item');
var logger = require('../logger')

var multer = require('multer');
var upload = multer({dest: __dirname});

var ItemRouter = express.Router();
ItemRouter.use(ExpressHelper.checkAuthorizedAPI);

ItemRouter.post('/submit', bodyParser.json(), function(req, res) {
  console.log(req.body);
  var item = new Item(req.body);
  item.save(function(err, dbItem) {
    if (err) {
      logger.warn(err)
      res.status(500).send(err);
    } else {
      res.send(JSON.stringify(dbItem));
    }
  });
});

ItemRouter.get('/fetch', function(req, res) {
  //Serve all available items from the DB.
  Item.find({new: false}, function(err, items) {
    if (err) {
      res.status(500).send(err);
    } else {
      res.send(items);
    }
  });
});

ItemRouter.post('/import', upload.single('inputFile'), function(req, res) {
  fs.readFile(req.file.path, function(err, data) {
    if (err) {
      logger.warn('Cannot read %s file!', req.file.path);
    } else {
      ItemHelper.importItems(data, true, function() {
        res.send('Parsing completed!');
        fs.unlink(req.file.path);
      });
    }
  });
});

// Export all items from the current db to the items.csv file
ItemRouter.get('/export/all-items.tsv', function(req, res) {
  var filename = __dirname + '/all-items.tsv';

  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });

  Item.find({}, function(err, items) {
    if (err) {
      res.status(500).send(err);
    } else {
      serveFile(filename, res, items);
    }
  });
});

ItemRouter.get('/export/new-items.tsv', function(req, res) {
  var filename = __dirname + '/new-items.tsv';

  res.set({
    'Content-Type': 'text/plain',
    'Content-Disposition': 'attachment'
  });

  Item.find({new: true}, function(err, items) {
    if (err) {
      res.status(500).send(err);
    } else {
      serveFile(filename, res, items);
    }
  });
});

function serveFile(filename, res, items) {
  var writeStream = fs.createWriteStream(filename);
  ItemHelper.exportItems(writeStream, items);
  writeStream.end();
  writeStream.on('finish', function() {
    res.sendFile(filename, function() {
      fs.unlink(filename);
    });
  });
}

module.exports = ItemRouter;
