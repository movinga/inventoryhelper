var express = require('express');
var InventoryHelper = require('../helpers/inventoryHelper');
var ExpressHelper = require('../helpers/expressHelper');
var Inventory = require('../models/inventory');
var Item = require('../models/item');
var logger = require('../logger.js');
var fs = require('fs');

var InventoryRouter = express.Router();
InventoryRouter.use(ExpressHelper.checkAuthorizedAPI);

// Find the invnetory with the requested oppID.
// Create a new one and return it if does not exist
InventoryRouter.get('/:oppId', function(req, res) {
  Inventory.findOne({oppId: req.params.oppId}, function(err, inventory) {
    if (inventory) {
      Item.populate(inventory.items, {path: 'item'}, function(err, items) {
        res.send(inventory);
      });
    } else {
      var newInventory = new Inventory({
        oppId: req.params.oppId,
        comment: "",
        totalVolume: 0,
        totalWeight: 0,
        totalAssemblyPrice: 0,
      });
      res.send(newInventory);
    }
  });
});

//Save (upsert) the updated inventory
InventoryRouter.post('/:oppId', function(req, res) {
  if (!req.body.inventory.hasOwnProperty('items')) {
    req.body.inventory.items = [];
  }
  Inventory.findOneAndUpdate({oppId: req.params.oppId}, req.body.inventory, {upsert: true}, function(err) {
    if (err) {
      res.status(500).send(err);
    } else {
      InventoryHelper.createInventoryReport(req.body.locale, req.params.oppId, function(filename) {
        InventoryHelper.triggerInventoryCallback(req.body.inventory, req.body.locale, req.app.get('i18next'));
        InventoryHelper.uploadInventoryReport(filename, req.body.locale, req.body.hash, function(filename) {
          fs.access(filename, fs.F_OK, function(err) {
            if (!err) {
              try {
                fs.unlink(filename);
              } catch(err) {
                logger.warn(err.message);
              }
            }
          });
        });
        res.send("200 OK");
      });
    }
  });
});

module.exports = InventoryRouter;
