//Rudimentary model for user account. Passwords are stored in plain text right now!
var Account = {
  accounts: [
    {username: "movinga", group: "user", password: "94reN6zp"},
    {username: "admin", group: "admin", password: "UDYAq3FN"},
  ],

  find: function(username) {
    for (var i = 0; i < this.accounts.length; i++) {
      if (this.accounts[i].username === username) {
        return this.accounts[i];
      }
    }
    return undefined;
  },

  verifyPassword: function(user, password) {
    return user.password === password;
  },
};

module.exports = Account;
