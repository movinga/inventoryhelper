var mongoose = require('mongoose');

var schema = new mongoose.Schema({
  name: {
    en: String,
    de: String,
    fr: String,
    it: String
  },
  type: String,
  new: Boolean,
  volume: Number, //(in cf)
  disassembledVolume: Number, //(in cf)
  weight: Number, //(in lb)
  assemblyPrice: Number //Assembly price (in GBP)
});

module.exports = mongoose.model('Item', schema);
