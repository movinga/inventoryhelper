var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var schema = new mongoose.Schema({
  oppId: String,
  comment: String,
  totalVolume: Number, /* in cf */
  totalWeight: Number, /* in lb */
  totalAssemblyPrice: Number, /* in GBP */
  items: [{
    item: {type: ObjectId, ref: 'Item'},
    quantity: Number,
    disassembly: Boolean,
    reassembly: Boolean
  }]
});

module.exports = mongoose.model('Inventory', schema);
