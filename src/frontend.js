global.jQuery = global.$ = require('jquery');
require('bootstrap');

var i18n = require('i18next');
var XHR = require('i18next-xhr-backend');

var React = require('react');
var ReactDOM = require('react-dom');
var Container = require('./components/container');

var locale = $('#locale').val();
var oppId = $('#oppId').val();

i18n.use(XHR).init({
  lng: locale,
  fallbackLng: 'en',
  backend: {
    loadPath: '/{{lng}}/{{ns}}.json',
    crossDomain: false
  }
}, function(err, t) {
  if (err) {
    console.error(err);
  }
  global.t = t;
  //Render React component
  ReactDOM.render(
    <Container locale={locale} oppId={oppId} />, document.getElementById('content')
  );
});
