var Reflux = require('reflux');
var InventoryActions = require('../actions/inventoryActions');

var UtilityHelper = require('../helpers/utilityHelper');
var round = UtilityHelper.round;

var InventoryStore = Reflux.createStore({
  inventory: {},

  init: function() {
    this.listenTo(InventoryActions.fetch.completed, this.onFetched);
    this.listenTo(InventoryActions.addItem, this.onAddItem);
    this.listenTo(InventoryActions.removeItem, this.onRemoveItem);
    this.listenTo(InventoryActions.updateItem, this.onUpdateItem);
  },

  getInitialState: function() {
    return this.inventory;
  },

  onFetched: function(inventory) {
    this.inventory = inventory;
    this.trigger(this.inventory);
  },

  /*
  recalculateTotals: function(invItem, deltaQuantity, deltaDisassembly, deltaReassembly) {
    if (deltaQuantity !== 0) {
      var volume = invItem.disassembly ? invItem.item.disassembledVolume : invItem.item.volume;
      this.inventory.totalVolume = round(this.inventory.totalVolume + volume * deltaQuantity, 2);
      this.inventory.totalWeight = round(this.inventory.totalWeight + invItem.item.weight * deltaQuantity, 2);
      this.inventory.totalAssemblyPrice = round(this.inventory.totalAssemblyPrice + invItem.item.assemblyPrice * deltaQuantity * (invItem.disassembly + invItem.reassembly), 2);
    } else if (deltaDisassembly !== 0) {
      this.inventory.totalVolume = round(this.inventory.totalVolume + (invItem.item.disassembledVolume - invItem.item.volume) * invItem.quantity * deltaDisassembly, 2);
      this.inventory.totalAssemblyPrice = round(this.inventory.totalAssemblyPrice + invItem.item.assemblyPrice * invItem.quantity * deltaDisassembly, 2);
    } else if (deltaReassembly !== 0) {
      this.inventory.totalAssemblyPrice = round(this.inventory.totalAssemblyPrice + invItem.item.assemblyPrice * invItem.quantity * deltaReassembly, 2);
    }
  },*/

  recalculateTotals: function() {
    var totalVolume = 0;
    var totalWeight = 0;
    var totalAssemblyPrice = 0;
    this.inventory.items.forEach(function(invItem) {
      if (invItem.disassembly) {
        totalVolume = round(totalVolume + invItem.item.disassembledVolume * invItem.quantity, 2);
      } else {
        totalVolume = round(totalVolume + invItem.item.volume * invItem.quantity, 2);
      }
      totalWeight = round(totalWeight + invItem.item.weight * invItem.quantity, 2);
      totalAssemblyPrice = round(totalAssemblyPrice + invItem.item.assemblyPrice * invItem.quantity * (invItem.disassembly + invItem.reassembly), 2);
    });
    this.inventory.totalVolume = totalVolume;
    this.inventory.totalWeight = totalWeight;
    this.inventory.totalAssemblyPrice = totalAssemblyPrice;
  },

  onAddItem: function(inventoryItem) {
    // Find item. If found, just increase the quantity
    var index = this.inventory.items.findIndex(function(element) {
      return element.item['_id'] === inventoryItem.item['_id'];
    });
    if (index !== -1) {
      this.inventory.items[index].quantity = this.inventory.items[index].quantity + inventoryItem.quantity;
    } else {
      this.inventory.items.unshift(inventoryItem);
    }
    //this.recalculateTotals(inventoryItem, inventoryItem.quantity, 0, 0);
    this.recalculateTotals();
    this.trigger(this.inventory);
  },

  onRemoveItem: function(index) {
    var deletedItem = this.inventory.items.splice(index, 1)[0];
    //this.recalculateTotals(deletedItem, -1 * deletedItem.quantity, 0, 0);
    this.recalculateTotals();
    this.trigger(this.inventory);
  },

  onUpdateItem: function(index, updateHash) {
    //updateHash always contains new values of quantity, disassembly and reassembly
    var deltaQuantity = updateHash.quantity - this.inventory.items[index].quantity;
    var deltaDisassembly = updateHash.disassembly - this.inventory.items[index].disassembly;
    var deltaReassembly = updateHash.reassembly - this.inventory.items[index].reassembly;
    this.inventory.items[index].quantity = updateHash.quantity;
    this.inventory.items[index].disassembly = updateHash.disassembly;
    this.inventory.items[index].reassembly = updateHash.reassembly;
    //this.recalculateTotals(this.inventory.items[index], deltaQuantity, deltaDisassembly, deltaReassembly);
    this.recalculateTotals();
    this.trigger(this.inventory);
  },
});

module.exports = InventoryStore;
