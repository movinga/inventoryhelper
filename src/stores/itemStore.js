var Reflux = require('reflux');
var ItemActions = require('../actions/itemActions');
var InventoryActions = require('../actions/inventoryActions');

var ItemStore = Reflux.createStore({
  items: [],

  init: function() {
    this.listenTo(ItemActions.fetch.completed, this.onFetched);
    this.listenTo(ItemActions.submit.completed, this.onSubmitted);
  },

  geiInitialState: function() {
    return (this.items);
  },

  onFetched: function(results) {
    console.log("Items fetched!");
    this.items = results;
    this.trigger(this.items);
  },

  onSubmitted: function(item) {
    console.log("Item successfully submitted!");
    InventoryActions.addItem({
      item: item,
      quantity: 1,
      disassembly: false,
      reassembly: false
    });
    this.items.push(item);
    this.trigger(this.items);
  }
});

module.exports = ItemStore;
