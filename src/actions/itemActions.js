var Reflux = require('reflux');

var ItemActions = Reflux.createActions({
  "fetch": {children: ["completed"]},
  "submit": {children: ["completed"]}
});

ItemActions.fetch.listen(function() {
  var thisAction = this;
  jQuery.ajax({
    type: 'GET',
    url: '/items/fetch',
    dataType: 'json',
    statusCode: {
      200: function(data) {
        this.completed(data);
      }.bind(this),
      500: function(err) {
        console.error(err);
      }
    }
  });
});

ItemActions.submit.listen(function(formData, callback) {
  var item = {
    name: {en: '', de: '', fr: '', it: ''},
    weight: 0,
    volume: 0,
    disassembledVolume: 0,
    assemblyPrice: 0,
    type: 'unit',
    new: true
  }
  item.name[formData.locale] = formData.name;

  jQuery.ajax({
    type: "POST",
    url: "/items/submit",
    data: JSON.stringify(item),
    dataType: 'json',
    contentType: "application/json; charset=utf-8",
    statusCode: {
      200: function(data) {
        this.completed(data);
        callback();
      }.bind(this),
      500: function(err) {
        console.error(err);
      }
    }
  });
});

module.exports = ItemActions;
