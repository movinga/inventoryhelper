var Reflux = require('reflux');
var oppId = $('#oppId').val();
var locale = $('#locale').val();
var hash = $('#hash').val();

var InventoryActions = Reflux.createActions({
  "fetch": {children: ["completed"]},
  "save": {},
  "addItem": {},
  "removeItem": {},
  "updateItem": {}
});

InventoryActions.fetch.listen(function() {
  $.ajax({
    type: 'GET',
    url: '/inventory/' + oppId,
    dataType: 'json',
    statusCode: {
      200: function(data) {
        this.completed(data);
      }.bind(this),
      500: function(err) {
        console.error(err);
      }
    }
  });
});

//TODO: Maybe move it to the store? Is it idiomatic?
InventoryActions.save.listen(function(inventory, comment) {
  inventory.comment = comment;
  $.ajax({
    type: 'POST',
    url: '/inventory/' + oppId,
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify({
      'inventory': inventory,
      'hash': hash,
      'locale': locale
    }),
    statusCode: {
      200: function() {
        alert('Save successful!');
      },
      500: function(err) {
        console.error(err);
      }
    }
  });
});

module.exports = InventoryActions;
