$("#importFileFormSubmitButton").click(function() {
  var formData = new FormData($('#importFileForm')[0]);
  $.ajax({
    url: '/items/import',
    type: 'POST',
    xhr: function() {
      var xhrSettings = $.ajaxSettings.xhr();
      if(xhrSettings.upload) {
        xhrSettings.addEventListener('progress', progressHandlingFunction, false)
      }
      return xhrSettings;
    },
    beforeSend: function() {
      $('#importFileProgress').show();
    },
    statusCode: {
      200: function() {
        alert('Upload successful!');
        $('#importFileModal').modal("hide");
        $('#importFileProgress').hide();
        $('progress').attr({value: 0, max: 1});
      },
      500: function(err) {
        console.error(err);
      }
    },
    data: formData,
    cache: false,
    contentType: false,
    processData: false
  });
});

function progressHandlingFunction(e) {
  if (e.lengthComputable) {
    $('progress').attr({value: e.loaded, max: e.total});
  }
}

function confirmChange(element) {
  return confirm('Are you sure you want to leave this page? Any unsaved progress will be lost!');
}
