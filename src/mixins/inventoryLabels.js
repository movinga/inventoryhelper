var UtilityHelper = require('../helpers/utilityHelper');
var ILHelper = require('../helpers/inventoryLocaleHelper');
var round = UtilityHelper.round;

function hasNewItems(inventory) {
  var hasNewItems = false;
  inventory.items.forEach(function(invItem) {
    if (invItem.item.new) {
      hasNewItems = true;
    }
  });
  return hasNewItems;
}

// Text label helpers for the whole Inventory.
module.exports = {
  // Generates localized text for total volume.
  // If there is at least 1 new item, add '+ ?' to the result.
  // Examples: '53 + ? cf', 81 m3
  totalVolumeText: function() {
    var volumeText = ILHelper.localizedTotalVolume(this.props.inventory, this.props.locale);
    if (hasNewItems(this.props.inventory)) volumeText += ' ± ?'
    return volumeText + ' ' + t('units.volume');
  },

  // Generates localized text for total weight.
  // If there is at least 1 new item, add '+ ?' to the result.
  // Examples: '53 + ? lb', 81 kg
  totalWeightText: function() {
    var weightText = ILHelper.localizedTotalWeight(this.props.inventory, this.props.locale);
    if (hasNewItems(this.props.inventory))  weightText += ' ± ?';
    return weightText + ' ' + t('units.weight');
  },

  // Generates localized text for total assembly price.
  // If there is at least 1 new item, add '+ ?' to the result.
  // Examples: '£53 + ?', €81
  totalAssemblyPriceText: function() {
    var assemblyPriceText = ILHelper.localizedTotalAssemblyPrice(this.props.inventory, this.props.locale);
    if (hasNewItems(this.props.inventory))  assemblyPriceText += ' ± ?';
    return t('units.currency') + assemblyPriceText;
  },
}
