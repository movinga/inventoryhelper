function createWorksheetFromArrayOfArrays(data) {
  var ws = {};
  var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
  for(var R = 0; R != data.length; ++R) {
    for(var C = 0; C != data[R].length; ++C) {
      if(range.s.r > R) range.s.r = R;
      if(range.s.c > C) range.s.c = C;
      if(range.e.r < R) range.e.r = R;
      if(range.e.c < C) range.e.c = C;
      var cell = {v: data[R][C]};
      if(cell.v == null) continue;
      var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

      if(typeof cell.v === 'number') cell.t = 'n';
      else if(typeof cell.v === 'boolean') cell.t = 'b';
      else if(cell.v instanceof Date) {
        cell.t = 'n'; cell.z = XLSX.SSF._table[14];
        cell.v = datenum(cell.v);
      }
      else cell.t = 's';

      ws[cell_ref] = cell;
    }
  }
  if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
  return ws;
}

function Workbook() {
  if(!(this instanceof Workbook)) return new Workbook();
  this.SheetNames = [];
  this.Sheets = {};
}

module.exports = {
  createWorksheet: function() {
    // Convert MongoDB representation to an array of arrays.
    var excelData = []
    excelData.push([
      t("inventoryTable.header.item"),
      t("inventoryTable.header.quantity"),
      t("units.unit.single"),
      t("inventoryTable.header.weight") + ", " + t("units.weight"),
      t("inventoryTable.header.volume") + ", " + t("units.volume"),
      t("inventoryTable.header.notes")
    ]);
    this.props.inventory.items.forEach(function(invItem) {
      excelData.push([
        invItem.item.name[this.props.locale],
        invItem.quantity,
        this.typeText(invItem, false),
        this.weightText(invItem, false),
        this.volumeText(invItem, false),
        this.notesText(invItem, true)
      ]);
    }.bind(this));
    excelData.push([
      t("inventoryTable.footer.total"),
      '',
      '',
      this.totalWeightText(),
      this.totalVolumeText(),
      this.totalAssemblyPriceText()
    ]);

    // Create Excel data structure
    var workbook = new Workbook();
    var worksheet = createWorksheetFromArrayOfArrays(excelData);
    workbook.SheetNames.push('Sheet 1');
    workbook.Sheets['Sheet 1'] = worksheet;

    // Save Excel file using FileSaver
    var workbookOptions = {bootType: "xlsx", bootSST: false, type: 'binary'};
    return XLSX.write(workbook, workbookOptions);
  }
}
