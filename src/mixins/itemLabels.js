// Text label helpers for items
module.exports = {
  // Generate localized item type text.
  // Examples: 'foot', 'units'
  itemTypeText: function(item, quantity) {
    var quantityKey = quantity > 1 ? 'plural' : 'single';
    var typeKey = item ? item.type : 'default';

    var translationKey = t(['units', typeKey, quantityKey].join('.'));
    var defaultTranslationKey = t(['units', 'default', quantityKey].join('.'));

    return translationKey ? translationKey : defaultTranslationKey;
  },
}
