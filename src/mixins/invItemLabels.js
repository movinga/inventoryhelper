var React = require('react'); // For JSX elements
var LocaleHelper = require('../helpers/localeHelper');
var ItemLabelsMixin = require('./itemLabels');

// Text label helpers for Inventory items.
module.exports = {
  // Generate localized notes text for inventory type.
  // Extended version - with price.
  notesText: function(invItem, extended) {
    var text = [];
    var assemblyPriceText = t('units.currency');
    if (invItem.item.new && invItem.item.assemblyPrice === 0) {
      assemblyPriceText += '?'
    } else {
      assemblyPriceText += LocaleHelper.currency(invItem.item.assemblyPrice, this.props.locale) * invItem.quantity;
    }
    var priceComponent = extended ? ' - ' + assemblyPriceText : '';

    if (invItem.disassembly) {
      text.push(<p className="lh60" key="0">{t('reportModal.notes.disassembly') + priceComponent}</p>);
    }
    if (invItem.reassembly) {
      text.push(<p className="lh60" key="1">{t('reportModal.notes.reassembly') + priceComponent}</p>);
    }

    return text;
  },

  // Generate localized volume text. Returns '?' if volume is not defined.
  // Extended version - with volume unit
  // Examples: '?', '5 cf'
  volumeText: function(invItem, extended) {
    var volumeText = "?"
    if (!invItem.item.new || invItem.item.volume !== 0) {
      if (invItem.disassembly) {
        volumeText = LocaleHelper.volume(invItem.item.disassembledVolume, this.props.locale);
      } else {
        volumeText = LocaleHelper.volume(invItem.item.volume, this.props.locale);
      }
    }

    return extended ? volumeText + " " + t("units.volume") : volumeText;
  },

  // Generate localized weight text. Returns '?' if weight is not defined.
  // Extended version - with weight unit
  // Examples: '? kg', '5.13 lb'
  weightText: function(invItem, extended) {
    var weightText = "?"
    if (!invItem.item.new || invItem.item.weight !== 0) {
      weightText = LocaleHelper.weight(invItem.item.weight, this.props.locale);
    }

    return extended ? weightText + " " + t("units.weight") : weightText;
  },

  // Generate localized quantity text.
  // Extended version - with quantity number in front (!!!)
  // Examples: '1 foot', '2 units', 'feet'
  typeText: function(invItem, extended) {
    var itemTypeText = ItemLabelsMixin.itemTypeText(invItem.item, invItem.quantity)
    return extended ? invItem.quantity + ' ' + itemTypeText : itemTypeText;
  },
}
